package structural.decorator;

interface Pizza {

    String getDescription();

    int getCost();
}

class SimplePizza implements Pizza {
    @Override
    public String getDescription() {
        return "simple pizza";
    }


    public int getCost() {
        return 50;
    }
}

class Margherita implements Pizza {
    @Override
    public String getDescription() {
        return "Margherita pizza";
    }


    public int getCost() {
        return 100;
    }
}

abstract class PizzaDecorator implements Pizza {

    protected Pizza pizza;

    public PizzaDecorator(Pizza pizza) {
        this.pizza = pizza;
    }


}

class FreshTomato extends PizzaDecorator {
    // we need a reference to obj we are decorating


    public FreshTomato(Pizza pizza) {
        super(pizza);
    }

    public String getDescription() {
        return pizza.getDescription() + ", Fresh Tomato ";
    }

    public int getCost() {
        return 40 + pizza.getCost();
    }
}

class Barbeque extends PizzaDecorator {
    public Barbeque(Pizza pizza) {
        super(pizza);
    }

    public String getDescription() {
        return pizza.getDescription() + ", Barbeque ";
    }

    public int getCost() {
        return 40 + pizza.getCost();
    }
}

public class Main {

    public static void main(String[] args) {
        Pizza pizza = new Margherita();
        Pizza pizza2 = new Barbeque(pizza);
        Pizza pizza3 = new FreshTomato(pizza2);

        System.out.println(pizza.getDescription());
        System.out.println(pizza2.getDescription());
        System.out.println(pizza3.getDescription());


        System.out.println(pizza.getCost());
        System.out.println(pizza2.getCost());
        System.out.println(pizza3.getCost());
    }
}
