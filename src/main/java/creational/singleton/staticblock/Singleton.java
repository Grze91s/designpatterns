package creational.singleton.staticblock;

public class Singleton {

    private static final Singleton singleton;

    static {
        try {
            singleton = new Singleton();
        } catch (Exception ex) {
            throw new RuntimeException("Exception occured in creating creational.singleton instance");
        }
    }

    private Singleton() {
    }

    public static Singleton getInstance() {
        return singleton;
    }

    public static void main(String[] args) {
        System.out.println(Singleton.getInstance());
        System.out.println(Singleton.getInstance());
    }
}
