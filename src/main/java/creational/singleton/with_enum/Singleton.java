package creational.singleton.with_enum;

enum EnumSingleton {
    INSTANCE;
}

public class Singleton {
    public static void main(String[] args) {
        System.out.println(EnumSingleton.INSTANCE);
        System.out.println(EnumSingleton.INSTANCE);
    }
}