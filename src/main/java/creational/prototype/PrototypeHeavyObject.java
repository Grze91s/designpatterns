package creational.prototype;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PrototypeHeavyObject implements Cloneable {
    private List<String> aLotsOfData;

    public PrototypeHeavyObject(List<String> aLotsOfData) {
        this.aLotsOfData = aLotsOfData;
    }

    public PrototypeHeavyObject() {
        this.aLotsOfData= Collections.emptyList();
    }

    public void loadData() {
        //read all employees from database and put into the list
        this.aLotsOfData=Stream.iterate(0, n -> n + 1).limit(5).map(String::valueOf).collect(Collectors.toList());
    }

    public List<String> getaLotsOfData() {
        return aLotsOfData;
    }
    @Override
    public Object clone(){
        ArrayList<String> clonedStrings = new ArrayList<>(this.aLotsOfData);
        return new PrototypeHeavyObject(clonedStrings);
    }

    public static void main(String[] args) {
        PrototypeHeavyObject prototypeHeavyObject=new PrototypeHeavyObject();
        prototypeHeavyObject.loadData();

        PrototypeHeavyObject clone = (PrototypeHeavyObject) prototypeHeavyObject.clone();

        List<String> strings1 = prototypeHeavyObject.getaLotsOfData();
        List<String> strings2 = clone.getaLotsOfData();

        strings2.remove(0);
        strings1.add("0000");

        System.out.println(strings1);
        System.out.println(strings2);
    }
}
