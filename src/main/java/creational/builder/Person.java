package creational.builder;

public class Person {
    private final String name;
    private final String address;
    private final String gender;
    private final String employment;
    private final String homeOwnerStatus;

    private Person(String name, String address, String gender, String employment, String homeOwnerStatus) {
        this.name = name;
        this.address = address;
        this.gender = gender;
        this.employment = employment;
        this.homeOwnerStatus = homeOwnerStatus;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getGender() {
        return gender;
    }

    public String getEmployment() {
        return employment;
    }

    public String getHomeOwnerStatus() {
        return homeOwnerStatus;
    }

    public static class PersonBuilder {
        private String nestedName;
        private String nestedAddress;
        private String nestedGender;
        private String nestedEmploymentStatus;
        private String nestedHomeOwnerStatus;

        public PersonBuilder(
                final String newFullName,
                final String newAddress) {
            this.nestedName = newFullName;
            this.nestedAddress = newAddress;
        }

        public PersonBuilder name(final String newName) {
            this.nestedName = newName;
            return this;
        }

        public PersonBuilder address(final String newAddress) {
            this.nestedAddress = newAddress;
            return this;
        }

        public PersonBuilder gender(final String newGender) {
            this.nestedGender = newGender;
            return this;
        }

        public PersonBuilder employment(final String newEmploymentStatus) {
            this.nestedEmploymentStatus = newEmploymentStatus;
            return this;
        }

        public PersonBuilder homeOwner(final String newHomeOwnerStatus) {
            this.nestedHomeOwnerStatus = newHomeOwnerStatus;
            return this;
        }

        public Person createPerson() {
            return new Person(
                    nestedName, nestedAddress, nestedGender,
                    nestedEmploymentStatus, nestedHomeOwnerStatus);
        }
    }
}
