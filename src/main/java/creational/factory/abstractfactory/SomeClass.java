package creational.factory.abstractfactory;

/////////////////////////the different products
interface Wheel {
    void turn();
}

class RaceCarWheel implements Wheel {
    public void turn() {
        //    some stuff
    }
}

/////////////////////////the different products
interface Engine {
    void work();
}

class PowerfulEngine implements Engine {
    public void work() {
        //    some stuff
    }
}

/////////////////////////the factory
interface CarFactory {
    Engine getEngine();

    Wheel getWheel();
}

class SportCarFactory implements CarFactory {
    public Engine getEngine() {
        return new PowerfulEngine();
    }

    public Wheel getWheel() {
        return new RaceCarWheel();
    }
}

/////////////////////////some code using the factory
public class SomeClass {
    void someFunctionInTheCode() {
        CarFactory carFactory = new SportCarFactory();
        Wheel myWheel = carFactory.getWheel();
        Engine myEngine = carFactory.getEngine();
    }
}