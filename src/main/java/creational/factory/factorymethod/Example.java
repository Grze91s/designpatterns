package creational.factory.factorymethod;


interface Iterator<E> {
    boolean hasNext();

    E next();

    void remove();
}

//here comes the factory interface!
interface Iterable<T> {
    Iterator<T> iterator();
}

//here is a simplified definition of ArrayList from the java source code
//you can see that this class is a concrete factory that implements
//a factory method  iterator()
//Note : in the real Java source code, ArrayList is derived from
//AbstractList which is the one that implements the factory method pattern
class ArrayList<E> {
    //the iterator() returns a subtype and an "anonymous" Iterator<E>
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public E next() {
                return null;
            }

            @Override
            public void remove() {

            }
            //implementation of the methods hasNext(), next() and remove()
        };
    }
}

public class Example {
    public static void main(String[] ars) {
        //instantiation of the (concrete factory) ArrayList
        ArrayList<Integer> myArrayList = new ArrayList<>();
        //calling the factory method iterator() of ArrayList
        Iterator<Integer> myIterator = myArrayList.iterator();
    }
}
