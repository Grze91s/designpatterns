package creational.factory.staticfactorymethod;

public class MyClass {
    Integer a;
    Integer b;

    MyClass(int a, int b){
        this.a=a;
        this.b=b;
    };

    public static MyClass getInstance(int a, int b){
        return new MyClass(a, b);
    }

    public static void main(String[] args){
        //instanciation with a constructor
        MyClass a = new MyClass(1, 2);
        //instanciation with a static factory method
        MyClass b = MyClass.getInstance(1, 2);
    }
}